# ARSnova WebSocket Gateway

This service provides the WebSocket Gateway for the ARSnova ecosystem.

Starting it: `mvn spring-boot:run`